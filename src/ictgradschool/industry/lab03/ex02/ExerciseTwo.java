package ictgradschool.industry.lab03.ex02;

import ictgradschool.Keyboard;

/**
 * 1 Write a program that prompts the user to enter a range – 2 integers representing a lower bound and an upper bound.
 * You should use Keyboard.readInput() for this. Then, convert these bounds from String to int using Integer.parseInt().
 * Your program should then use Math.random() to generate 3 random integers that lie between the range entered (inclusive),
 * and then use Math.min() to determine which of the random integers is the smallest.
 */
public class ExerciseTwo {

    /**
     * TODO Your code here. You may also write additional methods if you like.
     */

     private void start() {
         System.out.println ("Please enter a lower bound number ");
         int lowerBound = Integer.parseInt(Keyboard.readInput());
         System.out.println ("Please enter a lower bound number ");
         int upperBound = Integer.parseInt(Keyboard.readInput());

         int range = upperBound-lowerBound + 1;
         int randomNumber1 = (int)(Math.random() * range + lowerBound);
         int randomNumber2 = (int)(Math.random() * range + lowerBound);
         int randomNumber3 = (int)(Math.random() * range + lowerBound);
         int smallestNumber = Math.min(Math.min(randomNumber1, randomNumber2),randomNumber3);
         System.out.println("3 randomly generated numbers: " + randomNumber1 + "," + randomNumber2 + "and" +randomNumber3);
         System.out.println("smallest number is" + smallestNumber);
    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        ExerciseTwo ex = new ExerciseTwo();
        ex.start();

    }
}
